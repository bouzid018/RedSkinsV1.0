﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RedSkins.Web.Startup))]
namespace RedSkins.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
